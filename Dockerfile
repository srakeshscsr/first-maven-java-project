FROM openjdk:8
EXPOSE 3000
ADD target/maven-unit-test.jar maven-unit-test.jar 
ENTRYPOINT ["java","-jar"."/maven-unit-test.jar"]
